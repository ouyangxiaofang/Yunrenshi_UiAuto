# -*- coding: utf-8 -*-
from Common.basePage import BasePage
from Common.readConfig import ini
from Utils.logger import log
from PageElement.Login.loginElement import LoginElement as LO

class LoginPage(BasePage):
    def login(self):
        self.driver.get(ini.url+LO.url)
        log.info("打开登录页面")
        # 等待输入框出现
        self.wait_elevisible(LO.username, doc='用户名输入框')
        try:
            username=ini.get_value('username')
            password=ini.get_value('password')
            log.info("进行登录,用户名:{} 密码:{}".format(username, password))
            # 输入用户名、密码进行登录
            self.input_text(LO.username, value=username, doc='用户名输入框')
            self.input_text(LO.password, value=password, doc='密码输入框')
            self.click(LO.btn, doc="登陆按钮")
            # 等待登录完成
            self.driver.implicitly_wait(10)
            if self.is_element_exsits(LO.company_name, doc="商户名称") == True:
                log.info("登录成功")
            return self.driver
        except Exception as e:
            print(e)

    def login_out(self):
        pass



