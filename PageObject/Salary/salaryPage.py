from PageElement.Salary.salaryElement import SalaryElement as SL
from Common.basePage import BasePage
from Common.readConfig import ini
from Utils.logger import log
class SalaryPage(BasePage):
    def index(self):
        log.info("打开无卡发薪页面")
        self.driver.get(ini.url+SL.url)
        return self.driver.title