# -*- coding:UTF-8 -*-
import os,glob
from shutil import copy
from Common.readConfig import ini
from Config.conf import cm
import pytest
from Utils.send_mail import Mail

def main():
    #配置运行环境
    env = 'pre'
    ini._set('ENV', 'env', env)
    # 拷贝 启动服务器脚本(Config/Start_server.bat)，由config目录拷贝到allure目录下进行压缩打包发送邮件
    copy(cm.Start_server_bat, cm.ALLURE_REPORT_DIR)
    #删除allure缓存文件00000011
    for infile in glob.glob(os.path.join(cm.REPORT_DIR,'allure_tmp','*')):
        if os.path.exists(infile):
            os.remove(infile)
    # 运行测试用例  -s : 打印信息 -v ：输出更详细的用例执行信息 -m：运行符合标签的用例
    pytest.main(["-vs",cm.TEST_CASE_DIR, "--alluredir=Report/allure_tmp"])
    os.system("allure generate Report/allure_tmp -o Report/allure --clean")
    # !!!
    #发送邮件
    Mail().send_report()  # 发送邮件
    #发送钉钉


if __name__ == "__main__":
    main()
